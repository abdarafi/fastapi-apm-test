FROM abdarafi/fastapi-em:0.1

# Install requirements
COPY ./requirements.txt /app/
RUN pip install -r requirements.txt

# Copy source code
COPY . /app
