from typing import Optional

from fastapi import FastAPI

from elasticapm.contrib.starlette import make_apm_client, ElasticAPM

#setup Elastic APM Agent
apm = make_apm_client({
    # Nama servis
    'SERVICE_NAME': 'CobaGan',
    # APM Server URL
    'SERVER_URL': 'http://192.168.1.102:8200',
    # Ignore Health Check
    'TRANSACTIONS_IGNORE_PATTERNS': ['^GET /health'],
    # Disable unwanted metrics
    'METRICS_SETS': "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
})
app = FastAPI()
app.add_middleware(ElasticAPM, client=apm)

@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}
